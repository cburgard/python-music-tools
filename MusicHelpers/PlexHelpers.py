operators_from_xsp = {
    "contains":"__contains",
    "is":"__exact",
    "not_is":"__exact!",
    "not_contains":"__contains!",
}

fields_from_xsp = {
    "artist":"artist.title",
    "album":"album.title",    
    "genre":"artist.genre"
}


def trackinfo(track,include_ref=False):
    artist = track.artist().title
    if not artist:
        print("track "+track.title+" has no artist!")        
    info = {
        "title":str(track.title if track.title else track.titleSort),
        "time":str(track.duration),
        "mood":str(track.moods),                
        "guid":str(track.guid),
        "album":str(track.parentTitle),
        "genres":str(track.artist().genres),
        "genre":" ".join(list(map(str,track.artist().genres))),        
        "artist":str(artist),
    }
    if include_ref:
        info["_track"] = track
    return info

def get_session(timeout):
    import requests
    class TimeoutAdapter(requests.adapters.HTTPAdapter):
        def __init__(self, timeout=None, *args, **kwargs):
            self.timeout = timeout
            super(TimeoutAdapter, self).__init__(*args, **kwargs)

        def send(self, *args, **kwargs):
            kwargs['timeout'] = self.timeout
            return super(TimeoutAdapter, self).send(*args, **kwargs)

    s = requests.Session()
    s.mount("http://", TimeoutAdapter(timeout=timeout))
    s.mount("https://", TimeoutAdapter(timeout=timeout))
    return s

def convert_xsp(xsp):
    filters = []
    for rule in xsp["rules"]:
        fieldname = fields_from_xsp[rule["field"]]
        filters.append({fieldname:rule["text"]})
    op = "or"
    if xsp["match"] == "all":
        op = "and"
    return {op:filters}

def sanitize(s):
    return s.strip().replace("'","’")
