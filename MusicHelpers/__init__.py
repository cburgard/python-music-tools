#!/bin/env python

def mkdir(path):
    """ensure that a directory exists, creating superdirectories if necessary. behaves like UNIX 'mkdir -p'."""
    import pathlib
    from os.path import isdir
    if not isdir(path):
        pathlib.Path(path).mkdir(parents=True)
        
def get_files(path):
    """put all the files in the given directory into a list"""
    from os import walk,listdir
    from os.path import join as pjoin
    return [pjoin(root,filename) for root,subdirs,files in walk(path) for filename in files]

def recode(s):
    """recode a string to get rid of nasty unicode characters"""
    import sys
    if sys.version_info < (3,0):
        if isinstance(s, str):
            return s.decode('ascii', 'ignore').encode('ascii') #note: this removes the character and encodes back to string.
        elif isinstance(s, unicode):
            return s.encode('ascii', 'ignore')
    else:
        return s

def sanitize(s,forceASCII=False):
    """sanitize a string such that it will be accepted as a filename by most file systems"""
    prelim = s.replace(":","_").replace("!","_").replace("\\","_").replace("*","_").replace("?","_").replace('"',"").replace("<","_").replace(">","_").replace("|","_").replace("&","and").replace("/","-")
    if forceASCII:
        return ''.join([i for i in prelim if ord(i)<128])
    return prelim

def get_local_music_from_files(paths):
    """read all your ID3 tag information from the given list of files to a python dictionary with filenames as keys"""    
    from mutagen.mp3 import MP3
    from mutagen.easyid3 import EasyID3
    from mutagen.mp3 import HeaderNotFoundError
    localmusic = {}
    for musicfile in paths:
        try:
            audio = {}
            mp3 = MP3(musicfile, ID3=EasyID3)
            for k in mp3:
                audio[k] = recode(mp3[k][0])
            localmusic[musicfile] = audio
        except HeaderNotFoundError:
            print("get_local_music: error reading file header for '{:s}'".format(musicfile))
    print("done retrieving local music list")
    return localmusic

def get_local_music(path):
    """read all your ID3 tag information from the given path to a python dictionary with filenames as keys"""
    print("reading local music from "+path)
    return get_local_music_from_files(get_files(path))

def map_local_music(music,music_lookup,music_lookup_by_path = {} ,duplicate_tracks = []):
    """create a data structure that is easily searchable for the local music"""
    for path,d in music.items():
        artist = d.get("artist","Unknown Artist")
        album =  d.get("album","Unknown Album")
        title = d.get("title","Unknown Title")
        genre = d.get("genre","")
        try:
            trackno = int(d.get("tracknumber","").split("/")[0])
        except ValueError:
            trackno = 0
        if not artist in music_lookup.keys():                music_lookup[artist] = {}
        if not album  in music_lookup[artist].keys():        music_lookup[artist][album] = {}
        music_lookup_by_path[path] = {"artist":artist,"album":album,"title":title,"trackno":trackno,"genre":genre}
        if not (trackno,title)  in music_lookup[artist][album].keys():
            music_lookup[artist][album][(trackno,title)] = path
        else:
            duplicate_tracks.append((path,music_lookup[artist][album][(trackno,title)]))
  
def write_m3u(ofname,contents):
    n = 0
    with open(ofname,"w") as outfile:
      for track in contents:
        outfile.write(track+"\n")
        n = n+1
    return n

def read_m3u(filename):
    with open(filename,"rt") as infile:
        return [ line.strip() for line in infile ]

def parse_xsp(xsp):
    """read an XSP file and return it as a python datastructure"""
    import xml.etree.ElementTree as ET
    tree = ET.parse(xsp)
    root = tree.getroot()

    order = root.find("order")
    if order is not None and order.text == "random":
        random = True
    else:
        random = False
        
    return {
        "name": root.find("name").text,
        "match": root.find("match").text,
        "random": random,
        "rules": [ {"text":rule.text, "field":rule.get("field"), "operator":str(rule.get("operator")).strip()} for rule in root.findall("rule") ]
    }


def evaluate_xsp(musicdict,playlist):
    """return the keys of all entries in the music dictionary where the values match the criteria in the XSP playlist"""
   
    def evaluate(key,pat,op):
        if op == "contains":
            if pat in key: return True
            else: return False
        elif op == "is":
            if pat == key: return True
            else: return False
        elif op == "not_is":
            if pat == key: return False
            else: return True            
        elif op == "not_contains":
            if pat in key: return False
            else: return True            
        else:
            raise RuntimeError("unsupported operator type: '{:s}'".format(op))
    
    def collapse(items,op):
        if op == "one":
            return any(items)
        elif op == "all":
            return all(items)
        else:
            raise RuntimeError("unsupported match type: '{:s}'".format(playlist["match"]))

    name = playlist["name"]
    if not name:
        raise RuntimeError("no name!")

    result = []
    for key,audio in musicdict.items():
        bools = []
        for rule in playlist["rules"]:
            bools.append(evaluate(audio[rule["field"]],rule["text"],rule["operator"]))
        match = collapse(bools,playlist["match"])
        if match:
            result.append(key)

    if playlist["random"]:
        from random import shuffle
        shuffle(result)
    else:
        result.sort()

    return {"name":name,"tracks":result }
    
def isclose(a,b,rel_tol=1e-9,abs_tol=1e-9):
    return abs(a-b) <= max( rel_tol * max(abs(a), abs(b)), abs_tol )


def similarString(stringa,stringb,match_unknown=False):
    if not stringa and not stringb:
#        print("passing "+stringa+" and "+stringb+" for emptyness!")        
        return True
    if not stringa or not stringb: return False
    if stringa == stringb:
#        print("passing "+stringa+" and "+stringb+" for equality!")
        return True
    from fuzzywuzzy import fuzz
    import re
    sa = re.sub("[\(\[].*?[\)\]]", "", stringa)
    sb = re.sub("[\(\[].*?[\)\]]", "", stringb)
    if not sa or not sb: return False
    if fuzz.ratio(sa,sb) > 80:
#        print("passing '"+stringa+"' and '"+stringb+"' for similarity!")
        return True
    if match_unknown:
        # NO comparison against unknown for stringa here
        if "Various" in stringb or "Unknown" in stringb:
#            print("passing '"+stringa+"' and '"+stringb+"' for unknown!")            
            return True
    return False

def similarTrack(tracka,trackb):
    titleOK  = similarString(tracka["title"], trackb["title"], False)
    artistOK = similarString(tracka["artist"],trackb["artist"],True)
    lengthOK = isclose(float(tracka["time"]),float(trackb["time"]),rel_tol=0.01,abs_tol=1)
#    albumOK  = similarString(tracka["album"], trackb["album"], True)
#    if titleOK+artistOK+albumOK > 1:
#        print(tracka,trackb,titleOK,artistOK,albumOK)
    return titleOK and artistOK and lengthOK


