#def get_remote_music(reader):
#    print("retrieving remote music list")
#    music = reader.get_all_songs()
#    print("done retrieving remote music list")
#    return music

def get_remote_music(reader):
    print("retrieving remote music list")
    music = reader.get_uploaded_songs()
    print("done retrieving remote music list")
    return music


def map_remote_music(music,music_lookup,music_lookup_by_id = {} ,duplicate_tracks = []):
    """create a data structure that is easily searchable for the remote music"""
    from MusicHelpers import recode
    for track in music:
        artist = recode(track.get("artist",""))
        album = recode(track.get("album",""))
        title = recode(track.get("title",""))
        genre = recode(track.get("genre",""))
        trackno = str(track["track_number"] if "track_number" in track.keys() else "")
        trackid = track["id"]
        music_lookup_by_id[trackid] = {"artist":artist,"album":album,"title":title,"trackno":trackno,"genre":genre}
        if not artist in music_lookup.keys():                music_lookup[artist] = {}
        if not album  in music_lookup[artist].keys():        music_lookup[artist][album] = {}
        if not (trackno,title) in music_lookup[artist][album].keys():
            music_lookup[artist][album][(trackno,title)] = trackid
        else:
            duplicate_tracks.append((trackid,music_lookup[artist][album][(trackno,title)]))
