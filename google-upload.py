#!/bin/env python2

from MusicHelpers import get_local_music,recode,mkdir

def main(args):
    from uuid import getnode as getmac
    from gmusicapi import Musicmanager
    loader = Musicmanager()
    loader.login()
    print("uploading:")
    for t in args.tracks:
        print("  "+t)
    retval = loader.upload(args.tracks, enable_matching=True, enable_transcoding=True, transcode_quality=u'320k')
    print(retval)
    
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="helper script to upload one or more tracks via the Google-Music-Manager python API")
    parser.add_argument("tracks",nargs="+",type=str,help="path to the music file")
    main(parser.parse_args())
