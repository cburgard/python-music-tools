#!/bin/env python2

def main(args):
    from uuid import getnode as getmac
    from gmusicapi import Mobileclient,Musicmanager
    print("authenticating as "+args.account+" on "+str(getmac()))
    reader = Mobileclient()
    if not reader.login(args.account,args.password,Mobileclient.FROM_MAC_ADDRESS):
        raise RuntimeError("unable to login with given credentials!")
    loader = Musicmanager()
    loader.login()

    from os.path import join as pjoin
    from MusicHelpers import get_local_music,map_local_music,mkdir,evaluate_xsp,write_m3u
    from MusicHelpers.GoogleMusicHelpers import get_remote_music,map_remote_music    
            
    local_music = get_local_music(args.path)
    remote_music = get_remote_music(loader)

    # create a data structure that is easily searchable for the local music
    local_music_lookup = {}
    local_music_lookup_by_path = {}        
    map_local_music(local_music,local_music_lookup,local_music_lookup_by_path)

    # create the same data structure for the remote music
    remote_music_lookup = {}
    remote_music_lookup_by_id = {}        
    map_remote_music(remote_music,remote_music_lookup,remote_music_lookup_by_id)

    remote_playlists = reader.get_all_user_playlist_contents()
    listids = { playlist["name"] : playlist["id"] for playlist in remote_playlists }
    
    if args.download:
        mkdir(args.output)
        for playlist in remote_playlists:
            name = playlist["name"]
            m3u = []
            for track in playlist["tracks"]:
                trackid = track["trackId"]
                try:
                    track = remote_music_lookup_by_id[trackid]
                except KeyError:
                    print("inconsistency detected, track "+trackid+" is missing!")
                    continue
                try:
                    path = local_music_lookup[track["artist"]][track["album"]][(track["trackno"],track["title"])]
                    m3u.append(path)
                except KeyError:
                    pass
            print("writing "+name)
            write_m3u(pjoin(args.output,name+".m3u"),m3u)

    if args.upload:
        for infile in args.input:
            if infile.endswith(".xsp"):
                try:
                    playlist = evaluate_xsp(remote_music_lookup_by_id,infile)
                    name = playlist["name"]
                    if name in listids.keys():
                        oldlistid = listids[name]
                        reader.delete_playlist(oldlistid)

                    print("making list "+name)
                    listid = reader.create_playlist(name)
                    reader.add_songs_to_playlist(listid,playlist["tracks"][:1000])
                except RuntimeError as err:
                    print("error in "+infile+": "+err.message)
                    continue
            else:
                print("file format not yet supported: "+infile)    

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="helper script to sync music with GoogleMusic via the Google-Music-Manager python API")
    parser.add_argument("path",type=str,help="path to the music folder")
    parser.add_argument("--print",dest="print_actions",action="store_true",help="print the actions to be done")
    parser.add_argument("--upload",dest="upload",action="store_true",help="upload playlists to GoogleMusic")
    parser.add_argument("--download",dest="download",action="store_true",help="download playlists missing on the local disk")
    parser.add_argument("--output",dest="output",default=".",type=str,help="folder to contain the downloaded playlists")
    parser.add_argument("--input",dest="input",nargs="+",default=[],help="playlists to be uploaded")            
    parser.add_argument("--account",type=str,required=False,default=None,help="Google account name. If not given, you will be asked to enter it.")
    parser.add_argument("--password",type=str,required=False,default=None,help="Google account password. If not given, you will be asked to enter it.")
    args = parser.parse_args()

    if not args.account:
        args.account = input("Google Account: ")
    if not args.password:
        import getpass
        args.password = getpass.getpass("Password for " + args.account + ": ")
    
    main(args)
    
