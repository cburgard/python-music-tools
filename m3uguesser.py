#!/bin/env python2

def match_elements(list1,list2,minscore):
    """match to lists of strings fuzzily"""
    from fuzzywuzzy import fuzz
    nmatch = 0
    list2copy = [ x for x in list2 ]
    for t1 in list1:
        bestmatchscore = 0
        bestmatch = None
        for t2 in list2copy:
            matchscore = fuzz.ratio(t1,t2)
            if matchscore > bestmatchscore:
                bestmatch = t2
                bestmatchscore = matchscore
        if bestmatchscore > minscore:
            list2copy.remove(bestmatch)
            nmatch = nmatch+1
    return nmatch

def main(args):
    from MusicHelpers import get_local_music,mkdir,read_m3u,write_m3u
    local_music = get_local_music(args.musicfolder)

    mkdir(args.output)

    from os.path import splitext,basename   
    from os.path import join as pjoin
    for m3ufile in args.m3u:
        base = splitext(basename(m3ufile))[0]
        outname = pjoin(args.output,base+".m3u")
        m3u = read_m3u(m3ufile)
        result = []
        for track in m3u:
            parts = track.split("/")
            secondlastlevel = parts[-3].split(" - ")
            lastlevel = parts[-2].split(" - ")
            filename = splitext(parts[-1])[0]
            trackname = filename.strip(" 0123456789-")
            elements = [trackname]+lastlevel+secondlastlevel
            for song,tags in local_music.items():
                matched = match_elements(elements,tags.values(),80)
                if matched > 2:
                    print("matched "+track+" && "+song)
                    result.append(song)
                    continue
        write_m3u(result,outname)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="update M3U files to new file locations using fuzzy matching of filenames to ID3 tag information")
    parser.add_argument("musicfolder",type=str,help="path to the music folder")
    parser.add_argument("--m3u",nargs="+",type=str,help="one or more m3u playlists",required=True)
    parser.add_argument("--output",type=str,required=True,help="output directory for the playlists")
    args = parser.parse_args()

    main(args)


