# Python Music Helpers

This package contains some scripts to help you manage your music collection using a command-line-interface. It requires the following packages:
 * `mutagen`
 * `xml.etree`

This project is heavily ispired by other projects:
 * [google-music-playlist-scripts](https://github.com/kennyszub/google-music-playlist-scripts)
 * [google-music-dupe-killer](https://github.com/maxkirchoff/google-music-dupe-killer)
 * [soulfx/gmusic-playlist](https://github.com/soulfx/gmusic-playlist)
 * [cvzi/itunes_smartplaylist](https://github.com/cvzi/itunes_smartplaylist)
 
However, neither of these implementations provide consistent access to all features in one suite with a consistent usage or CLI, so the purpose of this package is to do exactly that.

## xsp2m3u

Convert an XSP file to an M3U file using ID3 tag information.

### positional arguments
 * `musicfolder`          path to the music folder

### optional arguments:
  * `-h`, `--help`           show this help message and exit
  * `--xsp XSP [XSP ...]`  one or more smart playlists
  * `--output OUTPUT`      output directory for the playlists


## google-sync

This helper script requires the following packages
 * `gmusicapi`
 
 You can use it to syncronize the contents of some folder in your file system with the contents of your GoogleMusic collection. In principle, it fulfills the same functionality as the GoogleMusicManager - however, it has a couple of advantages:
 * it's capable of detecting and deleting duplicates both online and on disk
 * it can recognize tracks that are already present and does not re-download them

Also, it's fairly simple code, so feel free to adjust it to your likings.

### positional arguments:
 * tracks                  list of tracks to be uploaded

### optional arguments:
 * `-h`, `--help`            show this help message and exit
 * `--print`               print the actions to be done
 * `--delete-remote-duplicates` delete duplicates from GoogleMusic
 * `--delete-local-duplicates` delete duplicates from local disk
 * `--upload`              upload tracks missing on GoogleMusic
 * `--download`            download tracks missing on the local disk
 * `--account ACCOUNT`     Google account name. If not given, you will be asked
                        to enter it.
 * `--password PASSWORD`   Google account password. If not given, you will be
                        asked to enter it.

## google-upload

This helper script requires the following packages
 * `gmusicapi`
 
 You can use it to upload one or more specified tracks to your Google Music cloud.

Also, it's fairly simple code, so feel free to adjust it to your likings.

### positional arguments:
 * path                  path to the music folder

### optional arguments:
 * `-h`, `--help`            show this help message and exit

## m3uguesser

This script can help you update M3U files to new file locations. This is usually a pain, as many pieces of music management software take it into their how hands to change filenames and/or ID3 tags, which often invalidates older playlists in M3U format. When you have an M3U playlist that no longer matches your music folder layout, you can use this script.

What it does is to attempt to match the filename given in the M3U playlist to ID3 tag information in your new library and come up with a corresponding file list matching your new layout. This is accomplished by using fuzzy string matching from the python modules `fuzzywuzzy` and `Levenshtein`, which should be installed for this package to work as intended.

### positional arguments:
*  `musicfolder`          path to the music folder

### optional arguments:
*  `-h`, `--help`           show this help message and exit
*  `--m3u M3U [M3U ...]`  one or more m3u playlists
*  `--output OUTPUT`      output directory for the playlists

