#!/bin/env python2

def main(args):
    from MusicHelpers import get_local_music,mkdir,evaluate_xsp,write_m3u
    
    mkdir(args.output)

    from os.path import join as pjoin

    local_music = get_local_music(args.musicfolder)

    for xsp in args.xsp:
        try:
            playlist = evaluate_xsp(local_music,xsp)
        except RuntimeError as err:
            print("error in "+xsp+": "+err.message)
            continue

        outname = pjoin(args.output,playlist["name"]+".m3u")
        print("writing "+outname)
        write_m3u(outname,playlist["tracks"])

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="convert an XSP file to an M3U file using ID3 tag information")
    parser.add_argument("musicfolder",type=str,help="path to the music folder")
    parser.add_argument("--xsp",nargs="+",type=str,help="one or more smart playlists",required=True)
    parser.add_argument("--output",type=str,required=True,help="output directory for the playlists")
    args = parser.parse_args()

    main(args)

