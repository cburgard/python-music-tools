#!/bin/env python2

from MusicHelpers import get_local_music,map_local_music,recode,mkdir,sanitize
from MusicHelpers.GoogleMusicHelpers import get_remote_music,map_remote_music

def main(args):
    from uuid import getnode as getmac
    from gmusicapi import Mobileclient,Musicmanager
    print("authenticating as "+args.account+" on "+str(getmac()))
#    reader = Mobileclient()
#    if not reader.login(args.account,args.password,Mobileclient.FROM_MAC_ADDRESS):
#        raise RuntimeError("unable to login mobile client with given credentials!")
    loader = Musicmanager()
    loader.login()

    remote_music = get_remote_music(loader)
    # create the same data structure for the remote music
    duplicate_remote_tracks = []    
    remote_music_lookup = {}
    remote_music_lookup_by_id = {}        
    map_remote_music(remote_music,remote_music_lookup,remote_music_lookup_by_id,duplicate_remote_tracks)

    local_music = get_local_music(args.path)
    # create a data structure that is easily searchable for the local music
    duplicate_local_tracks = []
    local_music_lookup = {}
    local_music_lookup_by_path = {}        
    map_local_music(local_music,local_music_lookup,local_music_lookup_by_path,duplicate_local_tracks)
          
    # initialize the output data structures
    missing_local_tracks = []
    missing_remote_tracks = []
    matches = {}
    # make the forward search
    for artist in local_music_lookup.keys():
        for album in local_music_lookup[artist].keys():
            for trackno,title in local_music_lookup[artist][album].keys():
                try:
                    matches[remote_music_lookup[artist][album][(trackno,title)]] = local_music_lookup[artist][album][(trackno,title)]
                except KeyError:
                    missing_remote_tracks.append(local_music_lookup[artist][album][(trackno,title)])
    # make the backward search
    for artist in remote_music_lookup.keys():
        for album in remote_music_lookup[artist].keys():
            for trackno,title in remote_music_lookup[artist][album].keys():
                try:
                    if not matches[remote_music_lookup[artist][album][(trackno,title)]] == local_music_lookup[artist][album][(trackno,title)]:
                        raise RuntimeError("insconsistency detected!")
                except KeyError:
                    missing_local_tracks.append(remote_music_lookup[artist][album][(trackno,title)])                    

    # print a summary
    print("\n\n\na total of "+str(len(matches))+" tracks have been matched successfully!")       

    if len(missing_local_tracks) > 0:
        if args.print_actions:
            print("\n\n\nthe following remote tracks need to be downloaded")        
            for t in missing_local_tracks:
                track = remote_music_lookup_by_id[t]
                print(t + ": "+track["artist"]+"/"+track["album"]+"/"+track["trackno"]+" "+track["title"])        
        if args.download:
            print("downloading {:d} tracks...".format(len(missing_local_tracks)))
            from os.path import join as pjoin
            for t in missing_local_tracks:
                track = remote_music_lookup_by_id[t]                
                print("downloading "+track["artist"]+"/"+track["album"]+"/"+track["trackno"]+" "+track["title"])        
                filename, audio = loader.download_song(t)
                path = pjoin(args.path,sanitize(track["artist"]),sanitize(track["album"]))
                mkdir(path)
                outpath = pjoin(path,sanitize(filename,True))
                print("  writing to "+outpath)
                with open(outpath, 'wb') as f:
                    f.write(audio)
            
    if len(missing_remote_tracks) > 0:
        if args.print_actions:
            print("\n\n\nthe following local tracks are missing on the server and need to be uploaded")
            for t in missing_remote_tracks:
                print(t)
        if args.upload:
            print("uploading {:d} tracks...".format(len(missing_remote_tracks)))
            loader.upload(missing_remote_tracks, enable_matching=True, enable_transcoding=True, transcode_quality=u'320k')

    if len(duplicate_local_tracks) > 0:
        if args.print_actions:
            print("\n\n\nduplicate tracks detected locally:")
            for path1,path2 in duplicate_local_tracks:
                track = local_music_lookup_by_path[path1]
                print(track["artist"]+"/"+track["album"]+"/"+track["trackno"]+" "+track["title"]+": \n\t" + path1 + "\n\t" +path2)
        if args.delete_local_duplicates:
            print("deleting local duplicates")
            for path1,path2 in duplicate_local_tracks:
                from os import remove
                if len(path1) > len(path2):
                    print("removing "+path1)
                    remove(path1)
                else:
                    print("removing "+path2)
                    remove(path2)

    if len(duplicate_remote_tracks) > 0:
        if args.print_actions:
            print("\n\n\nduplicate tracks detected on server:")
            for id1,id2 in duplicate_remote_tracks:
                track = remote_music_lookup_by_id[id1]
                print(track["artist"]+"/"+track["album"]+"/"+track["trackno"]+" "+track["title"]+": \n\t" + id1 + "\n\t" +id2)        
        if args.delete_remote_duplicates:
            duplicates = [ id2 for id1,id2 in duplicate_remote_tracks ]
            print("deleting "+str(len(duplicates))+" remote duplicates")
            reader.delete_songs(duplicates)
        
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="helper script to sync music with GoogleMusic via the Google-Music-Manager python API")
    parser.add_argument("path",type=str,help="path to the music folder")
    parser.add_argument("--print",dest="print_actions",action="store_true",help="print the actions to be done")
    parser.add_argument("--delete-remote-duplicates",dest="delete_remote_duplicates",action="store_true",help="delete duplicates from GoogleMusic")
    parser.add_argument("--delete-local-duplicates",dest="delete_local_duplicates",action="store_true",help="delete duplicates from local disk")
    parser.add_argument("--upload",dest="upload",action="store_true",help="upload tracks missing on GoogleMusic")
    parser.add_argument("--download",dest="download",action="store_true",help="download tracks missing on the local disk")    
    parser.add_argument("--account",type=str,required=False,default=None,help="Google account name. If not given, you will be asked to enter it.")
    parser.add_argument("--password",type=str,required=False,default=None,help="Google account password. If not given, you will be asked to enter it.")
    args = parser.parse_args()

    if not args.account:
        args.account = raw_input("Google Account: ")
    if not args.password:
        import getpass
        args.password = getpass.getpass("Password for " + args.account + ": ")
    
    main(args)
