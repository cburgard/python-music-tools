#!/bin/env python2


def server(account,name,token):
    for resource in account.resources():
        if resource.provides == "server" and resource.name == name:
            for connection in resource.connections:
                if connection.relay: continue
                if connection.address.startswith("172."): continue
                from plexapi.myplex import PlexServer
                return PlexServer(connection.httpuri,token)

def load_music(server,music,thedict,allow_cache=False):
    # some caching, mostly to speed up debugging
    import pickle
    if not "info" in thedict.keys() and allow_cache and isfile(server+".tracks.pkl"):
        print("retrieving tracks (from cache: "+server+".tracks.pkl)")
        with open(server+".tracks.pkl","rb") as infile:
            thedict["info"] = pickle.load(infile)
    if not "info" in thedict.keys():
        print("retrieving tracks")
        from MusicHelpers.PlexHelpers import trackinfo
        thedict["music"] = music
        thedict["audio"] = thedict["music"].search(libtype='track')
        thedict["info"] = [ trackinfo(e) for e in thedict["audio"] ]
        thedict["lookup"] = { track.guid:track for track in thedict["audio"] }
        with open(server+".tracks.pkl","wb") as outfile:        
            pickle.dump(thedict["info"],outfile)
    
def main(args):
    from time import sleep    
    from os.path import basename,splitext,isfile
    from plexapi.myplex import MyPlexAccount
    from plexapi.exceptions import NotFound
    from MusicHelpers.PlexHelpers import sanitize,get_session

    session = get_session(60)
    account = MyPlexAccount(args.account, args.password, session=session)
    
    target = server(account,args.server,account.authenticationToken)
    targetmusic = target.library.section("Music")
    target_music_info = {}

    target_lists = target.playlists()

    if args.clear_all:
        for pl in target_lists:
            pl.delete()
        target_lists = []

    if args.sync:
        source = server(account,args.sync,account.authenticationToken)
        source_lists = source.playlists()
        target_list_names = [pl.title for pl in target_lists]

        from MusicHelpers import similarTrack
        from MusicHelpers.PlexHelpers import trackinfo

        for pl in source_lists:
            sleep(1)
            title = str(pl.title)
            if pl.smart:
                if args.allow_smart:
                    for playlist in target.playlists():
                        if playlist.title == title:
                            playlist.delete()
                    try:
                        filters = pl.filters()["filters"]
                        ok = False
                        for f in filters.values():
                            if len(f) > 0:
                                ok = True
                        if ok:
                            targetmusic.createPlaylist(title=title,smart=True,filters=filters)
                            print("created "+title)
                        else:
                            print("skipping playlist "+title+" with empty filters")
                    except NotFound as err:
                        print("failed to create "+title+": "+str(err))
            else:
                newpl = []
                load_music(args.server,targetmusic,target_music_info)
                for item in [ trackinfo(e,include_ref=True) for e in pl.items() ]:
                    newitem = None
                    if not "local" in item["guid"]:
                        for targetitem in target_music_info["info"]:
                            if targetitem["guid"] == item["guid"]:
                                newitem = targetitem
                    if not newitem:
                        for targetitem in target_music_info["info"]:
                            if similarTrack(targetitem,item):
                                newitem = targetitem
                                break
                    if not newitem:
                        print("unable to find "+item["title"])
                    else:
                        newpl.append(target_music_info["lookup"][newitem["guid"]])
                if len(newpl) == len(pl):
                    if pl.title in target_list_names:
                        target_list = [ sl for sl in target_lists if sl.title == pl.title ][0]
                        if len(target_list) == len(newpl):
                            print("skipping playlist "+pl.title+", seems to be present already with "+str(len(newpl))+" items!")
                            continue
                        else:
                            print("deleting playlist "+pl.title+" from target due to conflict!")                                                        
                            target_list.delete()
                    print("creating playlist "+title+" with "+str(len(newpl))+" items!")
                    playlist = target.createPlaylist(title=title,items=newpl)
                else:
                    print("unable to create playlist "+pl.title+", not all items found!")
                    
    if args.upload:
        from MusicHelpers import evaluate_xsp,parse_xsp,read_m3u
        from MusicHelpers.PlexHelpers import convert_xsp
        for infile in args.upload:
            if infile.endswith(".xsp"):
                xsp = parse_xsp(infile)
                name = xsp["name"]
                for playlist in target.playlists():
                    if playlist.title == name:
                        playlist.delete()
                if args.allow_smart:
                    targetmusic.createPlaylist(title=name,smart=True,filters=convert_xsp(xsp))
                else:
                    load_music(args.server,targetmusic,target_music_info)                    
                    playlist = evaluate_xsp({t["guid"]:t for t in target_music_info["info"]},xsp)
                    tracks  = []
                    for guid in playlist["tracks"]:
                        tracks.append(target_music_info["lookup"][guid])
                    if len(tracks) > 0:
                        print("creating playlist "+name+" with "+str(len(tracks))+" items!")
                        pl = target.createPlaylist(title=name,items=tracks)
                    else:
                        print("unable to create playlist "+name+", no items found!")                    
            elif infile.endswith(".m3u"):
                files = read_m3u(infile)
                items = get_local_music(files)
            elif infile.endswith(".txt"):
                with open(infile,"rt") as tracks:
                    name = splitext(basename(infile))[0]
                    print("processing "+infile+ " as "+name)
                    for playlist in target.playlists():
                        if playlist.title == name:
                            playlist.delete()                    
                    tracklist = []
                    for track in tracks:
                        parts = track.split("-")
                        artist = sanitize(parts[0])
                        title = sanitize("-".join(parts[1:]))
                        tracks = plexmusic.searchTracks(**{"track.title":title,"artist.title":artist})
                        if len(tracks) < 1:
                            print("unable to find: "+artist + "-" + title)
                            continue
                        track = tracks[0]
                        tracklist.append(track)
                    if len(tracklist) > 0:
                        playlist = target.createPlaylist(title=name,section=plexmusic,smart=False,items=tracklist)
            else:
                print("file format not yet supported: "+infile)                
#            
#    if args.download:
#        # create a data structure that is easily searchable for the local music
#        local_music_lookup = {}
#        local_music_lookup_by_path = {}        
#        map_local_music(local_music,local_music_lookup,local_music_lookup_by_path)
#        
#        # create the same data structure for the remote music
#        remote_music_lookup = {}
#        remote_music_lookup_by_id = {}        
#        map_remote_music(remote_music,remote_music_lookup,remote_music_lookup_by_id)
#        
#        remote_playlists = reader.get_all_user_playlist_contents()
#        listids = { playlist["name"] : playlist["id"] for playlist in remote_playlists }
#
#        mkdir(args.output)
#        for playlist in remote_playlists:
#            name = playlist["name"]
#            m3u = []
#            for track in playlist["tracks"]:
#                trackid = track["trackId"]
#                try:
#                    track = remote_music_lookup_by_id[trackid]
#                except KeyError:
#                    print("inconsistency detected, track "+trackid+" is missing!")
#                    continue
#                try:
#                    path = local_music_lookup[track["artist"]][track["album"]][(track["trackno"],track["title"])]
#                    m3u.append(path)
#                except KeyError:
#                    pass
#            print("writing "+name)
#            write_m3u(pjoin(args.output,name+".m3u"),m3u)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="helper script to sync music with GoogleMusic via the Google-Music-Manager python API")
    parser.add_argument("--path",type=str,help="path to the music folder")
    parser.add_argument("--print",dest="print_actions",action="store_true",help="print the actions to be done")
    parser.add_argument("--cache",dest="use_cache",action="store_true",help="use pickle to cache",default=False)
    parser.add_argument("--clear",dest="clear_all",action="store_true",help="clear all playlists",default=False)        
    parser.add_argument("--sync",dest="sync",default=None,help="sync playlist from another Plex server")    
    parser.add_argument("--download",dest="download",action="store_true",help="download playlists missing on the local disk")
    parser.add_argument("--output",dest="output",default=".",type=str,help="folder to contain the downloaded playlists")
    parser.add_argument("--upload",dest="upload",nargs="+",default=[],help="playlists to be uploaded")
    parser.add_argument("--smart-lists",dest="allow_smart", action="store_true",help="allow creation of smart lists",default=True)        
    parser.add_argument("--no-smart-lists",dest="allow_smart", action="store_false",help="disallow creation of smart lists")    
    parser.add_argument("--server",type=str,required=True,default=None,help="Plex server to talk to")
    parser.add_argument("--account",type=str,required=False,default=None,help="Plex account name. If not given, you will be asked to enter it.")
    parser.add_argument("--password",type=str,required=False,default=None,help="Plex account password. If not given, you will be asked to enter it.")
    args = parser.parse_args()

    if not args.account:
        args.account = input("Plex Account: ")
    if not args.password:
        import getpass
        args.password = getpass.getpass("Password for " + args.account + ": ")

    from requests.exceptions import ConnectionError
    from plexapi.exceptions import Unauthorized,BadRequest
        
    try:
        waittime = 2
        while True:
            try:
                main(args)
                break
            except BadRequest as ex:
                print("internal server error: "+str(ex)+", retrying...")
            except ConnectionError as ex:
                print("connectivity issues: "+str(ex)+", retrying... ")
            from time import sleep
            sleep(waittime)
            waittime *= 2
                
    except KeyboardInterrupt:
        print("interrupted - exiting gracefully")
        exit(0)
    except Unauthorized:
        print("invalid email, username or password")
        exit(0)

            
        
